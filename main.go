package main

import (
	"net/http"

	"github.com/labstack/echo"
	"html/template"
	"io"
	"log"
	"net"
	"os"
	"os/signal"
	"strconv"
	"strings"
)

type Template struct {
	templates *template.Template
}

func (t *Template) Render(w io.Writer, name string, data interface{}, c echo.Context) error {
	return t.templates.ExecuteTemplate(w, name, data)
}

func GetServerNum(n net.Addr) string {
	l, _, _ := net.SplitHostPort(n.String())

	s := strings.Split(l, ".")

	if len(s) == 1 {
		return "1"
	}

	i, err := strconv.Atoi(s[len(s)-1])

	if err != nil {
		return "1"
	}

	if i%10 > 0 && i%10 < 6 {
		return strconv.Itoa(i % 10)
	} else {
		return "1"
	}
}

func XffPresent(x map[string][]string) bool {
	_, xff := x["X-Forwarded-For"]
	_, xtrue := x["X-True-IP"]
	return xff || xtrue
}

func Secure(s string) string {
	if s == "https" {
		return "s"
	} else {
		return ""
	}
}

var servercolor = map[string]string{
	"http":  "0, 72, 146",
	"https": "149, 9, 36",
}

var maincolor = map[string]string{
	"http":  "49, 125, 203",
	"https": "192, 12, 48",
}

func Hello(c echo.Context) error {
	return c.Render(http.StatusOK, "hello", map[string]interface{}{
		"server":           c.Request().Context().Value(http.LocalAddrContextKey).(net.Addr),
		"servernum":        GetServerNum(c.Request().Context().Value(http.LocalAddrContextKey).(net.Addr)),
		"client":           c.Request().RemoteAddr,
		"vsname":           c.Request().Host,
		"headers":          c.Request().Header,
		"method":           c.Request().Method,
		"proto":            c.Request().Proto,
		"host":             c.Request().Host,
		"url":              c.Request().URL,
		"xffpresent":       XffPresent(c.Request().Header),
		"xffip":            c.RealIP(),
		"accesstype":       c.Scheme(),
		"secure":           Secure(c.Scheme()),
		"serverbackground": servercolor[c.Scheme()],
		"mainbackground":   maincolor[c.Scheme()],
	})
}

func ServerHeader(next echo.HandlerFunc) echo.HandlerFunc {
	return func(c echo.Context) error {
		c.Response().Header().Set(echo.HeaderServer, "Echo/3.0")
		return next(c)
	}
}

func main() {
	e := echo.New()

	e.Use(ServerHeader)

	e.Static("/static", "assets")

	t := &Template{
		templates: template.Must(template.ParseGlob("templates/*.html")),
	}

	e.Renderer = t
	e.GET("/", Hello)
	e.HEAD("/", Hello)

	sigs := make(chan os.Signal, 1)
	signal.Notify(sigs)

	go func() {
		s := <-sigs
		log.Printf("RECEIVED SIGNAL: %s", s)
		os.Exit(1)
	}()

	go e.Start(":8080")
	e.StartTLS(":8443", "cert.pem", "key.pem")
}